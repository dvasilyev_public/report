import json
import re
from datetime import datetime
from uuid import uuid4

import pandas as pd
from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy
from markupsafe import Markup
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, UniqueConstraint, not_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import relationship, backref
from werkzeug.exceptions import abort

from diff_json import DiffType, Comparator

app = Flask(__name__, template_folder="templates")
app.secret_key = "secret key"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite3"
db = SQLAlchemy(app)


class ReportTable(db.Model):
    id = Column(Integer, primary_key=True)
    created_ts = Column(DateTime, nullable=False, default=datetime.utcnow)
    title = Column(String(80))
    convert = Column(String)


class MessageTable(db.Model):
    id = Column(Integer, primary_key=True)
    message_name_id = Column(String(36))
    first = Column(String)
    second = Column(String)
    report_id = Column(String(36), ForeignKey("report_table.id"), nullable=False)
    report = relationship("ReportTable", backref=backref("message", lazy=True))
    __table_args__ = (UniqueConstraint("report_id", "message_name_id"),)


class DiffTable(db.Model):
    id = Column(Integer, primary_key=True)
    diff_type = Column(String(8), nullable=False)
    path_dot = Column(String(200), nullable=False)
    path_dot_star = Column(String(200), nullable=False)
    first = Column(String)
    second = Column(String)
    message_id = Column(String(36), ForeignKey("message_table.id"), nullable=False)
    message = relationship("MessageTable", backref=backref("diff", lazy=True))


class Report:
    def __init__(self, tables, header):
        self.tables = tables
        self.header = header


class Convert:
    def __init__(self, config):
        self.config = config

    def get(self):
        if not self.config.convert:
            return None, None, None
        config = json.loads(self.config.convert)
        first = [(eval(i[0]), *i[1:]) for i in config["first"]]
        second = [(eval(i[0]), *i[1:]) for i in config["second"]]
        ignore = config["ignore"]
        return first, second, ignore


class ReportDataBase:
    def __init__(self, report_id: int = None):
        self.report_id = report_id

    def calculate(self):
        convert_first, convert_second, ignore = self._get_convert()
        for message_id, first, second in self._get_messages():
            comparator = Comparator(
                first=first,
                second=second,
                convert_first=convert_first,
                convert_second=convert_second,
                ignore=ignore,
            )
            self._extend_diff(message_id=message_id, diffs=comparator.compare())

    def get_report(self):
        tables = self._get_tables()
        header = self._get_header()
        return Report(tables=tables, header=header)

    def get_reports(self):
        for item in ReportTable.query.order_by(ReportTable.id.desc()).all():
            yield Markup(f'{item.created_ts} <a href="/report/{item.id}/">{item.title}</a>')

    def create_report(self, title: str = None, convert: str = None):
        report = ReportTable(title=title, convert=convert)
        db.session.add(report)
        db.session.commit()
        self.report_id = report.id

    def add_messages(self, messages):
        mess = (
            MessageTable(
                message_name_id=i[0],
                first=i[1],
                second=i[2],
                report_id=self.report_id,
            )
            for i in messages
        )
        try:
            db.session.add_all(mess)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            raise

    def get_message(self, message_name_id: str):
        result = (
            MessageTable.query.with_entities(MessageTable.first, MessageTable.second)
                .filter_by(report_id=self.report_id, message_name_id=message_name_id)
                .one()
        )
        return result

    def _get_tables(self) -> list:
        for diff_type, path_dot_star in self._get_diff_table_names():
            table_data = self._get_diff_table_data(diff_type, path_dot_star)
            length = table_data.count()
            table = Table(diff_type, path_dot_star, table_data, length)
            yield table

    def _get_header(self) -> str:
        messages = self._length_messages()
        messages_diff = self._length_diff_message_id()
        diff_class = self._length_diff_class()
        diffs = self._length_diffs()
        return f"messages={messages}, messages_diff={messages_diff}, diff_class={diff_class}, diffs={diffs}"

    def _get_messages(self):
        message_ids_done = (
            DiffTable.query.join(MessageTable)
                .filter(MessageTable.report_id == self.report_id)
                .with_entities(DiffTable.message_id)
                .subquery()
        )
        table = MessageTable.query.with_entities(MessageTable.id, MessageTable.first, MessageTable.second).filter(
            MessageTable.report_id == self.report_id, not_(MessageTable.id.in_(message_ids_done))
        )
        for item in table:
            yield item.id, json.loads(item.first), json.loads(item.second)

    def _get_convert(self):
        row = ReportTable.query.filter_by(id=self.report_id).one()
        convert = Convert(row)
        return convert.get()

    def _length_messages(self):
        return MessageTable.query.filter_by(report_id=self.report_id).count()

    @staticmethod
    def _stringify(data):
        return json.dumps(data, separators=(",", ":"), ensure_ascii=False)

    def _extend_diff(self, message_id, diffs):
        for item in diffs:
            row = DiffTable(
                message_id=message_id,
                diff_type=item.diff_type,
                path_dot=item.path_dot,
                path_dot_star=item.path_dot_star,
                first=self._stringify(item.first),
                second=self._stringify(item.second),
            )
            db.session.add(row)
        db.session.commit()

    def _length_diffs(self):
        return DiffTable.query.join(MessageTable).filter(MessageTable.report_id == self.report_id).count()

    def _length_diff_message_id(self):
        return (
            DiffTable.query.join(MessageTable)
                .filter(MessageTable.report_id == self.report_id)
                .with_entities(DiffTable.message_id)
                .distinct()
                .count()
        )

    def _get_diff_table_names(self):
        return (
            DiffTable.query.join(MessageTable)
                .filter(MessageTable.report_id == self.report_id)
                .with_entities(DiffTable.diff_type, DiffTable.path_dot_star)
                .distinct()
                .order_by(DiffTable.diff_type.asc(), DiffTable.path_dot_star.asc())
        )

    def _get_diff_table_data(self, diff_type, path_dot_star):
        return (
            DiffTable.query.join(MessageTable)
                .filter(
                MessageTable.report_id == self.report_id,
                DiffTable.diff_type == diff_type,
                DiffTable.path_dot_star == path_dot_star,
            )
                .with_entities(
                MessageTable.message_name_id,
                MessageTable.report_id,
                DiffTable.path_dot,
                DiffTable.first,
                DiffTable.second,
            )
        )

    def _length_diff_class(self):
        return self._get_diff_table_names().count()


class Table:
    def __init__(self, diff_type, path_dot_star, table_data, length):
        self.diff_type = diff_type
        self.data = self._get_html_table(table_data)
        self.name = f'diff_type="{diff_type}", path_dot_star="{path_dot_star}", length={length}'

    def _get_html_table(self, table_data):
        df = pd.DataFrame()
        for i in table_data:
            first = "" if self.diff_type == DiffType.add else i.first
            second = "" if self.diff_type == DiffType.remove else i.second
            _link = f'<a href="/diff/{i.report_id}/{i.message_name_id}/">{i.message_name_id}</a>'
            record = [_link, i.path_dot, f"{first}<br>{second}"]
            columns = ["message_name_id", "path_dot", "first<br>second"]
            df = df.append(pd.DataFrame([record], columns=columns), ignore_index=True)
        return Markup(df.to_html(render_links=True, escape=False, classes="table"))


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[-1] == "json"


@app.route("/status")
def status():
    return "ok"


@app.route("/", methods=["GET"])
def main_page():
    report_db = ReportDataBase()
    reports = report_db.get_reports()
    return render_template("main.html", reports=reports)


@app.route("/", methods=["POST"])
def upload_file():
    if ("filesFirst" not in request.files) or ("filesSecond" not in request.files):
        return redirect(request.url)
    else:
        convert = None
        if "convertConfig" in request.files:
            file = request.files.getlist("convertConfig")[0]
            if allowed_file(file.filename):
                convert = str(file.read(), "utf-8")

        report_db = ReportDataBase()
        title = request.form.get("reportTitle") or f"report {uuid4()}"
        report_db.create_report(title=title, convert=convert)
        for first in request.files.getlist("filesFirst"):
            if allowed_file(first.filename):
                message_name_id = re.findall(r"(\d+).json", first.filename)[0]
                for second in request.files.getlist("filesSecond"):
                    if allowed_file(second.filename) and message_name_id in second.filename:
                        first_data = str(first.read(), "utf-8")
                        second_data = str(second.read(), "utf-8")
                        mess = message_name_id, first_data, second_data
                        report_db.add_messages([mess])

        report_db.calculate()
        return redirect(url_for("main_page"))


@app.route("/api/v1/report", methods=["POST"])
def create_report():
    if not request.json:
        abort(400)
    report_db = ReportDataBase()
    title = request.json.get("title") or f"report {uuid4()}"
    convert = json.dumps(request.json["convert"]) if request.json.get("convert") else None
    report_db.create_report(title=title, convert=convert)
    return jsonify({"result": "ok", "id": report_db.report_id, "title": title}), 201


@app.route("/api/v1/upload", methods=["POST"])
def upload():
    if not request.json or not "report_id" in request.json:
        abort(400)
    report_db = ReportDataBase(report_id=request.json["report_id"])
    mess = ((i["id"], json.dumps(i["first"]), json.dumps(i["second"])) for i in request.json["data"])
    try:
        report_db.add_messages(mess)
    except IntegrityError:
        abort(400)
    report_db.calculate()
    return jsonify({"result": "ok"}), 201


@app.route("/report/<int:report_id>/")
def report_page(report_id):
    report_db = ReportDataBase(report_id=report_id)
    report = report_db.get_report()
    return render_template("report.html", report=report)


@app.route("/diff/<int:report_id>/<message_name_id>/")
def diff_page(report_id, message_name_id):
    report_db = ReportDataBase(report_id=report_id)
    first, second = report_db.get_message(message_name_id=message_name_id)
    return render_template("diff.html", json_1=first, json_2=second)


def init_db():
    # db.drop_all()  # fixme for debug
    db.create_all()


if __name__ == "__main__":
    init_db()
    app.run(host="localhost", port=8080)

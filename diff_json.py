from dataclasses import dataclass
from typing import Union, List, Iterable

NumTypes = (int, float)
LeafTypes = (str, *NumTypes, bool, type(None))
NodeTypes = (dict, list)
JsonTypes = (*NodeTypes, *LeafTypes)
PathType = List[Union[int, str]]


@dataclass
class DiffType:
    change: str = "change"
    add: str = "add"
    remove: str = "remove"


def _get_path_dot(path: PathType, stared=False) -> str:
    dot_path: str = "."
    for key in path:
        if isinstance(key, str):
            dot_path += key if dot_path == "." else f".{key}"
        elif isinstance(key, int):
            if stared:
                dot_path += "[*]"
            else:
                dot_path += f"[{key}]"
    return dot_path


class Diff:
    def __init__(
            self,
            diff_type: type(DiffType),
            path: PathType,
            first: Union[JsonTypes] = None,
            second: Union[JsonTypes] = None,
    ):
        self.diff_type = diff_type
        self.path = path
        self.first = first  # fixme check type
        self.second = second
        self.path_dot = _get_path_dot(path=path)
        self.path_dot_star = _get_path_dot(path=path, stared=True)

    def __repr__(self):
        return f'Diff(diff_type={self.diff_type}, path={self.path}, first={self.first}, second={self.second})'

    def __eq__(self, other):
        if not isinstance(other, Diff):
            return NotImplemented
        return (
                self.diff_type == other.diff_type
                and self.path == other.path
                and self.first == other.first
                and self.second == other.second
        )


class Comparator:
    def __init__(
            self,
            first: JsonTypes,
            second: JsonTypes,
            whole_array_compare: bool = True,
            convert_first: Iterable[tuple] = None,
            convert_second: Iterable[tuple] = None,
            ignore: Iterable[str] = None,
    ):
        self.first = first
        self.second = second
        self._whole_array_compare: bool = whole_array_compare
        self._ignore = ignore if ignore else []
        self._convert_first = convert_first if convert_first else []
        self._convert_second = convert_second if convert_second else []
        self._diffs: List[Diff] = []

    def compare(self) -> List[Diff]:
        # from test_diff_json import write_test_data
        # write_test_data(self.first, self.second)  # todo for dbg
        self._walk(first=self.first, second=self.second)
        return self._diffs

    def _append_diff_change(self, first_value: JsonTypes, second_value: JsonTypes, path: PathType) -> None:
        if _get_path_dot(path=path, stared=True) in self._ignore:
            return
        diff = Diff(diff_type=DiffType.change, first=first_value, second=second_value, path=path)
        self._diffs.append(diff)

    def _append_diff_add(self, second_value: JsonTypes, path: PathType) -> None:
        if _get_path_dot(path=path, stared=True) in self._ignore:
            return
        diff = Diff(diff_type=DiffType.add, second=second_value, path=path)
        self._diffs.append(diff)

    def _append_diff_remove(self, first_value: JsonTypes, path: PathType) -> None:
        if _get_path_dot(path=path, stared=True) in self._ignore:
            return
        diff = Diff(diff_type=DiffType.remove, first=first_value, path=path)
        self._diffs.append(diff)

    @staticmethod
    def _is_num(x) -> bool:
        return type(x) in NumTypes

    @staticmethod
    def _is_str(x) -> bool:
        return type(x) == str

    @staticmethod
    def _is_bool(x) -> bool:
        return type(x) == bool

    @staticmethod
    def _is_null(x) -> bool:
        return type(x) == type(None)

    @staticmethod
    def _is_object(x) -> bool:
        return type(x) == dict

    @staticmethod
    def _is_array(x) -> bool:
        return type(x) == list

    @staticmethod
    def _is_leaf(x) -> bool:
        return type(x) in LeafTypes

    @staticmethod
    def _is_node(x) -> bool:
        return type(x) in NodeTypes

    @staticmethod
    def _is_json_type(x) -> bool:
        return type(x) in LeafTypes or type(x) in NodeTypes

    def _compare_nums(self, first, second, path: PathType) -> None:
        if not self._is_num(second) or first != second:
            self._append_diff_change(first, second, path)

    def _compare_strs(self, first, second, path: PathType) -> None:
        if not self._is_str(second) or first != second:
            self._append_diff_change(first, second, path)

    def _compare_bools(self, first, second, path: PathType) -> None:
        if not self._is_bool(second) or first != second:
            self._append_diff_change(first, second, path)

    def _compare_nulls(self, first, second, path: PathType) -> None:
        if not self._is_null(second) or first != second:
            self._append_diff_change(first, second, path)

    def _is_arrays_comparable(self, first: list, second: list) -> bool:
        return all(self._is_leaf(i) for i in first) and all(self._is_leaf(i) for i in second)

    def _compare_arrays(self, first: list, second: list, path: PathType) -> None:
        if not self._is_array(second):
            self._append_diff_change(first, second, path)

        elif self._whole_array_compare and self._is_arrays_comparable(first, second):
            for array, convert in ((first, self._convert_first), (second, self._convert_second)):
                for index in range(len(array)):
                    array[index] = self._converted(array[index], path + [index], convert)
            if first != second:
                self._append_diff_change(first, second, path)

        else:
            for index in range(len(first)):
                first_value = first[index]
                if index < len(second):
                    second_value = second[index]
                    self._walk(first_value, second_value, path + [index])
                else:
                    self._append_diff_remove(first_value, path + [index])

            for index in range(len(second)):
                second_value = second[index]
                if index >= len(first):
                    self._append_diff_add(second_value, path + [index])

    def _compare_objects(self, first: dict, second: dict, path: PathType) -> None:
        if not self._is_object(second):
            self._append_diff_change(first, second, path)
        else:
            __value_not_exist = "!@#$%value_not_exist!@#$%"
            for key, first_value in first.items():
                second_value = second.get(key, __value_not_exist)
                if second_value is __value_not_exist:
                    self._append_diff_remove(first_value, path + [key])
                else:
                    self._walk(first_value, second_value, path + [key])

            for key, second_value in second.items():
                first_value = first.get(key, __value_not_exist)
                if first_value is __value_not_exist:
                    self._append_diff_add(second_value, path + [key])

    def _converted(self, element: LeafTypes, path: PathType, convert: Iterable[tuple]):
        for convert_func, *args in convert:
            convert_path = args[0]
            if convert_path == _get_path_dot(path=path, stared=True):
                try:
                    element = convert_func(element, *args[1:])
                except TypeError as e:
                    print(f"WARNING. Converter error, element path: {path} \n{e}")
        return element

    def _walk(self, first: JsonTypes, second: JsonTypes, path: PathType = None) -> None:
        if not path:
            path = []

        if not isinstance(first, JsonTypes):
            raise TypeError(f"No JSON type in first, field: {first}, path: {path}")
        if not isinstance(second, JsonTypes):
            raise TypeError(f"No JSON type in second, field: {second}, path: {path}")

        first = self._converted(first, path, self._convert_first)
        second = self._converted(second, path, self._convert_second)

        if self._is_num(first):
            self._compare_nums(first, second, path)
        elif self._is_str(first):
            self._compare_strs(first, second, path)
        elif self._is_bool(first):
            self._compare_bools(first, second, path)
        elif self._is_null(first):
            self._compare_nulls(first, second, path)
        elif self._is_object(first):
            self._compare_objects(first, second, path)
        elif self._is_array(first):
            self._compare_arrays(first, second, path)

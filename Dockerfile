FROM python:3.9-slim

WORKDIR /opt/app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY static static
COPY templates templates
COPY *.py ./
RUN python prepare.py

CMD gunicorn -w 4 -b 0.0.0.0:8080 wsgi:app

import pytest
from hamcrest import assert_that, equal_to

from diff_json import Diff, DiffType, Comparator


# count = 101
# def write_test_data(first, second):
#     global count
#     with open(f"first/output_{count}.json", "w") as f_out:
#         json.dump(first, f_out, separators=(',', ':'))
#         f_out.write("\n")
#     with open(f"second/response_{count}.json", "w") as f_out:
#         json.dump(second, f_out, separators=(',', ':'))
#         f_out.write("\n")
#     count += 1


@pytest.mark.parametrize(
    argnames="first, second",
    argvalues=[
        ("a", "a"),
        (1, 1),
        (1, 1.0),
        (1.1, 1.1),
        (False, False),
        (True, True),
        (None, None),
        ({"a": 1}, {"a": 1}),
        ({"a": ""}, {"a": ""}),
        ({"a": "a"}, {"a": "a"}),
        ([1, 2, 3], [1, 2, 3]),
        ({}, {}),
        ([], []),
    ],
)
def test_no_diff(first, second):
    comparator = Comparator(first=first, second=second)
    diffs = comparator.compare()
    assert_that(diffs, equal_to([]))


@pytest.mark.parametrize(
    argnames="first, second, expected_diff",
    argvalues=[
        # change type
        ({"a": 1.0}, {"a": 1.001}, Diff(diff_type=DiffType.change, path=["a"], first=1.0, second=1.001)),
        ({"a": 1}, {"a": "1"}, Diff(diff_type=DiffType.change, path=["a"], first=1, second="1")),
        ({"a": 1}, {"a": False}, Diff(diff_type=DiffType.change, path=["a"], first=1, second=False)),
        ({"a": 1}, {"a": True}, Diff(diff_type=DiffType.change, path=["a"], first=1, second=True)),
        ({"a": 1}, {"a": None}, Diff(diff_type=DiffType.change, path=["a"], first=1, second=None)),
        ({"a": ""}, {"a": "v"}, Diff(diff_type=DiffType.change, path=["a"], first="", second="v")),
        ({"a": "v"}, {"a": ""}, Diff(diff_type=DiffType.change, path=["a"], first="v", second="")),
        ({"a": None}, {"a": 1}, Diff(diff_type=DiffType.change, path=["a"], first=None, second=1)),
        ({"a": 1}, {"a": {}}, Diff(diff_type=DiffType.change, path=["a"], first=1, second={})),
        (
            {"a": 1},
            {"a": {"b": {"c": 1}}},
            Diff(diff_type=DiffType.change, path=["a"], first=1, second={"b": {"c": 1}}),
        ),
        (
            {"a": {"b": 1}},
            {"a": {"b": {"c": 1}}},
            Diff(diff_type=DiffType.change, path=["a", "b"], first=1, second={"c": 1}),
        ),
        ({"a": 1}, {"a": []}, Diff(diff_type=DiffType.change, path=["a"], first=1, second=[])),
        ({"a": 1}, {"a": [1, 2]}, Diff(diff_type=DiffType.change, path=["a"], first=1, second=[1, 2])),
        (
            {"a": [1, 2, 3]},
            {"a": [1, [1, 2], 3]},
            Diff(diff_type=DiffType.change, path=["a", 1], first=2, second=[1, 2]),
        ),
        ([1, 2], [1, {}], Diff(diff_type=DiffType.change, path=[1], first=2, second={})),
        ({"a": [1, 2, 3]}, {"a": 1}, Diff(diff_type=DiffType.change, path=["a"], first=[1, 2, 3], second=1)),
        (
            {"a": [1, "a"]},
            {"a": [1, "a", 3]},
            Diff(diff_type=DiffType.change, path=["a"], first=[1, "a"], second=[1, "a", 3]),
        ),
        (
            {"a": [1, "a", 3]},
            {"a": [1, "a"]},
            Diff(diff_type=DiffType.change, path=["a"], first=[1, "a", 3], second=[1, "a"]),
        ),
        # add type
        ({"a": 1}, {"a": 1, "b": 2}, Diff(diff_type=DiffType.add, path=["b"], second=2)),
        ({}, {"a": "1.999"}, Diff(diff_type=DiffType.add, path=["a"], second="1.999")),
        ({}, {"a": 1.999}, Diff(diff_type=DiffType.add, path=["a"], second=1.999)),
        ({}, {"a": False}, Diff(diff_type=DiffType.add, path=["a"], second=False)),
        ({}, {"a": True}, Diff(diff_type=DiffType.add, path=["a"], second=True)),
        ({}, {"a": None}, Diff(diff_type=DiffType.add, path=["a"], second=None)),
        ({}, {"a": {"b": 1}}, Diff(diff_type=DiffType.add, path=["a"], second={"b": 1})),
        ({}, {"a": [1, "a"]}, Diff(diff_type=DiffType.add, path=["a"], second=[1, "a"])),
        ({"a": {}}, {"a": {"b": 1}}, Diff(diff_type=DiffType.add, path=["a", "b"], second=1)),
        # remove type
        ({"a": 1, "b": 2}, {"a": 1}, Diff(diff_type=DiffType.remove, path=["b"], first=2)),
        ({"a": "1.999"}, {}, Diff(diff_type=DiffType.remove, path=["a"], first="1.999")),
        ({"a": 1.999}, {}, Diff(diff_type=DiffType.remove, path=["a"], first=1.999)),
        ({"a": False}, {}, Diff(diff_type=DiffType.remove, path=["a"], first=False)),
        ({"a": True}, {}, Diff(diff_type=DiffType.remove, path=["a"], first=True)),
        ({"a": None}, {}, Diff(diff_type=DiffType.remove, path=["a"], first=None)),
        ({"a": {"b": 1}}, {}, Diff(diff_type=DiffType.remove, path=["a"], first={"b": 1})),
        ({"a": [1, "a"]}, {}, Diff(diff_type=DiffType.remove, path=["a"], first=[1, "a"])),
        ({"a": {"b": 1}}, {"a": {}}, Diff(diff_type=DiffType.remove, path=["a", "b"], first=1)),
        (
            {"a": [True, False]},
            {"a": [False, True]},
            Diff(
                diff_type=DiffType.change,
                path=["a"],
                first=[True, False],
                second=[False, True],
            ),
        ),
    ],
)
def test_one_diff(first, second, expected_diff):
    comparator = Comparator(first=first, second=second)
    diffs = comparator.compare()
    assert_that(diffs, equal_to([expected_diff]))


@pytest.mark.parametrize(
    argnames="first, second, expected_diff",
    argvalues=[
        ({"a": [1, 2, 5]}, {"a": [1, 2]}, [Diff(diff_type=DiffType.remove, path=["a", 2], first=5)]),
        (
            {"a": ["a", "b"]},
            {"a": ["x", "b", "c"]},
            [
                Diff(diff_type=DiffType.change, path=["a", 0], first="a", second="x"),
                Diff(diff_type=DiffType.add, path=["a", 2], second="c"),
            ],
        ),
        (
            {"a": [True, False]},
            {"a": [False, True]},
            [
                Diff(diff_type=DiffType.change, path=["a", 0], first=True, second=False),
                Diff(diff_type=DiffType.change, path=["a", 1], first=False, second=True),
            ],
        ),
    ],
)
def test_every_list_item(first, second, expected_diff):
    comparator = Comparator(first=first, second=second, whole_array_compare=False)
    diffs = comparator.compare()
    assert_that(diffs, equal_to(expected_diff))


def test_several_diff():
    comparator = Comparator(
        first={"a": {"x": 1}, "b": 2, "c": [4, 5, 6]}, second={"a": {"x": 3}, "d": True, "c": [4, 5]}
    )
    diffs = comparator.compare()
    expected_diffs = [
        Diff(diff_type=DiffType.change, path=["a", "x"], first=1, second=3),
        Diff(diff_type=DiffType.remove, path=["b"], first=2),
        Diff(diff_type=DiffType.change, path=["c"], first=[4, 5, 6], second=[4, 5]),
        Diff(diff_type=DiffType.add, path=["d"], second=True),
    ]
    assert_that(diffs, equal_to(expected_diffs))


@pytest.mark.parametrize(
    argnames="first, second, expected_path_dot, expected_path_dot_star",
    argvalues=[
        ("a", "b", ".", "."),
        ({"a": 1}, {"a": 1, "b": 1}, ".b", ".b"),
        ({"a": 1}, {"a": 2}, ".a", ".a"),
        ([1, 2, 3, [], 5], [1, 2, 3, {}, 5], ".[3]", ".[*]"),
        ({"a": [[1], [2], [3]]}, {"a": [[1], [0], [3]]}, ".a[1]", ".a[*]"),
        ({"a": [1, {"b": [1, 2, 3]}, 3]}, {"a": [1, {"b": [1, 2, 0]}, 3]}, ".a[1].b", ".a[*].b"),
    ],
)
def test_path_dot(first, second, expected_path_dot, expected_path_dot_star):
    comparator = Comparator(first=first, second=second)
    diffs = comparator.compare()
    assert_that(diffs[0].path_dot, equal_to(expected_path_dot))
    assert_that(diffs[0].path_dot_star, equal_to(expected_path_dot_star))


@pytest.mark.parametrize(
    argnames="first, second, whole_array_compare, ignore, expected_diff",
    argvalues=[
        ({"a": [1, 2, 5]}, {"a": [1, 2]}, True, (".a",), []),
        ({".a.a": {"b.": {".c.": 1}}}, {".a.a": {"b.": {".c.": 2}}}, True, ("..a.a.b...c.",), []),
        ({"a": {"b": 1}}, {"a": {"b": 2}}, True, (".a.b",), []),
        ({"a": [1, 2, 3]}, {"a": [1, 2.2, 3]}, True, (".a",), []),
        ({"a": [1, 2, 3]}, {"a": [1, 2.2, 3]}, False, (".a[*]",), []),
        (
            {"a": {"b": 1, "c": 2}},
            {"a": {"b": 0, "c": 0}},
            True,
            (".a.b",),
            [Diff(diff_type=DiffType.change, path=["a", "c"], first=2, second=0)],
        ),
        (
            {"a": {"b": 1, "c": 2, "e": 2}},
            {"a": {"b": 0, "c": 0, "e": 0}},
            True,
            (".a.b", ".a.e"),
            [Diff(diff_type=DiffType.change, path=["a", "c"], first=2, second=0)],
        ),
    ],
)
def test_ignore(first, second, whole_array_compare, ignore, expected_diff):
    comparator = Comparator(first=first, second=second, whole_array_compare=whole_array_compare, ignore=ignore)
    diffs = comparator.compare()
    assert_that(diffs, equal_to(expected_diff))


@pytest.mark.parametrize(
    argnames="first, second, error_description",
    argvalues=[
        (object, {}, f"No JSON type in first, field: {object}, path: []"),
        ({}, object, f"No JSON type in second, field: {object}, path: []"),
        ({"a": 1}, {"a": object}, f"No JSON type in second, field: {object}, path: ['a']"),
    ],
)
def test_type_error(first, second, error_description):
    comparator = Comparator(first=first, second=second)
    with pytest.raises(TypeError) as e:
        comparator.compare()
    assert_that(str(e.value), equal_to(error_description))


@pytest.mark.parametrize(
    argnames="first, second, convert_first, convert_second, expected_diff",
    argvalues=[
        ({"af": 1}, {"af": "1"}, None, [(int, ".af")], []),
        ({"a": ["2", "1", "3"]}, {"a": ["1", "2", "3"]}, [(sorted, ".a")], None, []),
        (
            {"s": [{"a": [{"b": [1, 2]}, {"b": [2, 1]}]}, {"a": [{"b": [1, 2]}, {"b": [2, 1]}]}]},
            {"s": [{"a": [{"b": [2, 1]}, {"b": [1, 2]}]}, {"a": [{"b": [2, 1]}, {"b": [1, 2]}]}]},
            [(sorted, ".s[*].a[*].b")],
            [(sorted, ".s[*].a[*].b")],
            [],
        ),
        ({"a": [1, 2, 3]}, {"a": ["1", "2", "3"]}, None, [(float, ".a[*]")], []),
        ({"fr": 1.11}, {"fr": "1.111"}, [], [(float, ".fr"), (round, ".fr", 2)], []),
        ({"a": 1.11}, {"a": "1.111"}, [(float, ".a"), (int, ".a")], [(float, ".a"), (int, ".a")], []),
        (
            {"a": [1, 2, 5]},
            {"a": [2, 1]},
            None,
            [(sorted, ".a")],
            [Diff(diff_type=DiffType.change, path=["a"], first=[1, 2, 5], second=[1, 2])],
        ),
        (
            {"a": ["2", "1"]},
            {"a": [3, 2, 1]},
            [(float, ".a[*]"), (sorted, ".a")],
            [(sorted, ".a")],
            [Diff(diff_type=DiffType.change, path=["a"], first=[1, 2], second=[1, 2, 3])],
        ),
    ],
)
def test_convert(first, second, convert_first, convert_second, expected_diff):
    comparator = Comparator(first=first, second=second, convert_first=convert_first, convert_second=convert_second)
    diffs = comparator.compare()
    assert_that(diffs, equal_to(expected_diff))

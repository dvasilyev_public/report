import sys

import pytest

from main import app, init_db

sys.setrecursionlimit(2000)


@pytest.fixture(scope="session", autouse=True)
def client():
    yield app.test_client()


@pytest.fixture(scope="session", autouse=True)
def _setup():
    init_db()

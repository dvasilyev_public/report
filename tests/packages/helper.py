import glob
import io
import json
import re
from pathlib import Path
from urllib.parse import urlparse

from lxml import etree
from werkzeug.datastructures import FileStorage


def get_report_id(client, title):
    link = None
    response = client.get("/")
    assert response.status_code == 200
    for report_link in etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="list-item"]/a'):
        if report_link.text == title:
            link = report_link.attrib.get("href")
    assert link
    path = urlparse(link).path.strip("/")
    report_id = path.split("/")[-1]
    return report_id


def get_file(path):
    with open(path, "rb") as f:
        yield FileStorage(stream=io.BytesIO(f.read()), filename=Path(path).parts[-1], content_type="application/json")


def get_files(path_template):
    for path in glob.glob(path_template):
        yield from get_file(path)


def make_payload(report_id, chunk_size=999999):
    payload = {"report_id": report_id, "data": []}
    messages_1 = glob.glob('tests/first/*.json')

    for message_1 in messages_1:
        app_number = re.findall(r'\d+', message_1)[0]
        message_2 = glob.glob(f'tests/second/*{app_number}.json')[0]

        with open(message_1, "r", encoding='utf-8') as f1:
            with open(message_2, "r", encoding='utf-8') as f2:
                data = {"id": int(app_number), "first": json.loads(f1.read()), "second": json.loads(f2.read())}
                payload["data"].append(data)
                if len(payload["data"]) == chunk_size:
                    yield payload
                    payload["data"] = []
    yield payload

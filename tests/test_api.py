from uuid import uuid4

import pytest
from lxml import etree

from tests.packages.helper import make_payload


def test_report_create(client):
    title = f"test {uuid4()}"
    response = client.post("/api/v1/report", json={"title": title})
    assert response.status_code == 201

    response = client.get("/")
    assert response.status_code == 200
    report_link = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="list-item"]/a')[0]
    assert report_link.text == title


def test_upload_all(client):
    title = f"test {uuid4()}"
    response = client.post("/api/v1/report", json={"title": title})
    assert response.status_code == 201
    report_id = response.json["id"]

    for payload in make_payload(report_id):
        response = client.post("/api/v1/upload", json=payload)
        assert response.status_code == 201


@pytest.mark.parametrize(argnames="chunk_size", argvalues=(10, 2, 1))
def test_upload_chunks(client, chunk_size):
    title = f"test {uuid4()}"
    response = client.post("/api/v1/report", json={"title": title})
    assert response.status_code == 201
    report_id = response.json["id"]

    for payload in make_payload(report_id, chunk_size):
        response = client.post("/api/v1/upload", json=payload)
        assert response.status_code == 201


@pytest.mark.parametrize(argnames="chunk_size", argvalues=(10, 2, 1))
def test_report_equal_chunks_report(client, chunk_size):
    response = client.post("/api/v1/report", json={"title": f"test {uuid4()}"})
    assert response.status_code == 201
    report_id_chunked = response.json["id"]

    for payload in make_payload(report_id_chunked, chunk_size):
        response = client.post("/api/v1/upload", json=payload)
        assert response.status_code == 201

    response = client.post("/api/v1/report", json={"title": f"test {uuid4()}"})
    assert response.status_code == 201
    report_id = response.json["id"]

    payload = list(make_payload(report_id))[0]
    response = client.post("/api/v1/upload", json=payload)
    assert response.status_code == 201

    response = client.get(f"/report/{report_id_chunked}/")
    assert response.status_code == 200
    report_header_chunked = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="diff-list"]/h4')[0].text

    response = client.get(f"/report/{report_id}/")
    assert response.status_code == 200
    report_header = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="diff-list"]/h4')[0].text

    assert report_header == "messages=75, messages_diff=62, diff_class=21, diffs=71"
    assert report_header_chunked == report_header


def test_convert(client):
    _convert = {
        "first": [["sorted", ".s[*].a[*].b"]],
        "second": [["int", ".af"], ["float", ".fr"], ["round", ".fr", 2], ["sorted", ".s[*].a[*].b"]],
        "ignore": [".a.e"]
    }
    response = client.post("/api/v1/report", json={"title": f"test {uuid4()}", "convert": _convert})
    assert response.status_code == 201
    report_id = response.json["id"]

    payload = list(make_payload(report_id))[0]
    response = client.post("/api/v1/upload", json=payload)
    assert response.status_code == 201

    response = client.get(f"/report/{report_id}/")
    assert response.status_code == 200

    report_header = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="diff-list"]/h4')[0].text
    assert report_header == "messages=75, messages_diff=59, diff_class=17, diffs=64"

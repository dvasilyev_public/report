from uuid import uuid4

from hamcrest import assert_that, matches_regexp
from lxml import etree

from main import app
from tests.packages.helper import get_report_id, get_files

title = f"test {uuid4()}"


def setup_module():
    with app.test_client() as client:
        data = {
            "reportTitle": title,
            "filesFirst": [i for i in get_files("tests/first/*.json")],
            "filesSecond": [i for i in get_files("tests/second/*.json")],
            "convertConfig": [i for i in get_files("tests/test_data/*.json")],
        }
        response = client.post("/", data=data, follow_redirects=True, content_type="multipart/form-data")
        assert response.status_code == 200


def test_main(client):
    response = client.get("/")
    assert response.status_code == 200
    report_link = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="list-item"]/a')[0]
    assert report_link.text == title
    assert_that(report_link.xpath('@href')[0], matches_regexp("/report/\d+/"))


def test_report(client):
    report_id = get_report_id(client, title)
    response = client.get(f"/report/{report_id}/")
    assert response.status_code == 200
    report_header = etree.HTML(response.data.decode("utf-8")).xpath('//div[@class="diff-list"]/h4')[0].text
    assert report_header == "messages=75, messages_diff=59, diff_class=17, diffs=64"


def test_diff(client):
    report_id = get_report_id(client, title)
    response = client.get(f"/diff/{report_id}/175/")
    assert response.status_code == 200
    json_1 = etree.HTML(response.data.decode("utf-8")).xpath('//textarea[@id="editor-container-text"]')[0].text
    assert json_1.strip() == '{"a":["2","1"]}'
    json_2 = etree.HTML(response.data.decode("utf-8")).xpath('//textarea[@id="editor-container-text-2"]')[0].text
    assert json_2.strip() == '{"a":[3,2,1]}'

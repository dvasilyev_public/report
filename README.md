# report

### prepare

```
pip install -r requirements.txt
```

### python run local

```
python main.py
```

### test run

```
pytest tests
```

### docker run

[get VERSION](https://gitlab.com/dvasilyev_public/report/container_registry/1630164)

```
docker run --rm -d -p 8080:8080 --name app registry.gitlab.com/dvasilyev_public/report:VERSION
curl http://localhost:8080/
docker stop app
```

### data files for UI load

- message files must have valid json content:

- message file name have to match regesp: `*(\d+).json`

- first set files have to contain index like in second set, for example:

  first set: [`response_123.json`, `response_124.json`, `response_222.json`] 

  second set: [`message_123.json`, `message_124.json`, `message_222.json`] 

- convert config file must have valid json content, example:

    ```json
    {
      "convert_first": [["int", ".a"], ["float", ".b[*]"]],
      "convert_second": [["float", ".b[*]"]],
      "ignore": [".c"]
    }
    ```

- convert config file have to match regesp: `*.json`

### REST API

- create report 

    request:

    POST /api/v1/report
    ```json
    {
      "title": "test title",
      "convert": {
        "convert_first": [["int", ".a"], ["float", ".b[*]"]],
        "convert_second": [["float", ".b[*]"]],
        "ignore": [".c"]
      }
    }
    ```

    response:

    HTTP 201 Created  
    ```json
    {
      "result": "ok", "id": 1, "title": "test title"
    }
    ```
    
- add data to report

    request:

    POST /api/v1/upload
    ```json
    {
      "report_id": 1,
      "data": [
        {"id":123, "first":{"a": 1, "b":[1.1, 2.5]}, "second":{"a":"1", "b":["1.10", "2.50"], "c":"minor"}},
        {"id":124, "first":{"a": 1, "b":[1.2, 2.0]}, "second":{"a":"1", "b":["1.20", "2.00"], "c":"minor"}},
        {"id":222, "first":{"a": 1, "b":[1.3, 2.0]}, "second":{"a":"1", "b":["1.30", "2.00"], "c":"minor"}}
      ]
    }
    ```

    response:
    
    HTTP 201 Created
    ```json
    {
      "result": "ok"
    }
    ```
